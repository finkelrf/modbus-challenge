#!/usr/bin/env python3
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
import paho.mqtt.client as mqtt
import logging
from time import sleep
import json


# configure the client logging
FORMAT = (
    "%(asctime)-15s %(threadName)-15s "
    "%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s"
)
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)


class EcoAdapt():
    def __init__(self, address, port=5020):
        ''' Modbus client class '''
        self.client = ModbusClient(address, port=port)
        self.registers = [
            (0, 1, "Software Version"),
            (1, 1, "Modbus table version"),
            (2, 3, "MAC Address"),
            (244, 12, "Power factor"),
            (352, 12, "RMS voltage"),
            (388, 12, "RMS voltage 1 min average"),
            (424, 12, "Frequency")
        ]

    def read_registers(self):
        ''' Read sensor registers from via modbus  '''
        log.info("Setting up client")
        self.client.connect()
        readings = []

        log.info("Reading registers")
        for r in self.registers:
            resp = self.client.read_input_registers(r[0], r[1])
            log.info("%s: %s: %s" % (r, resp, resp.registers))
            readings.append((r[2], resp.registers))

        log.info("Closing client")
        self.client.close()
        
        return readings

class Mqtt():
    ''' Mqtt class '''
    def __init__(self, host, port=1883):
        self.mqtt_client = mqtt.Client()
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message

        self.mqtt_client.connect(host, port, 60)

    def loop(self):
        # Not necessary for this exercise because we are not subscribing to any topic
        self.mqtt_client.loop_forever()

    def on_connect(self, client, userdata, flags, rc):
        pass 

    def on_message(self, client, userdata, msg):
        pass

    def publish_ecoadapt_readings(self, readings):
        ''' Publish readings to mqtt '''
        for reading in readings:
            self.mqtt_client.publish(f"rpi0/ecoadapt/{reading[0]}", json.dumps(reading[1]))


if __name__ == "__main__":
    mqtt = Mqtt("mqtt-broker")
    ecoadapt = EcoAdapt("modbus-server")
    
    # Publish sensor reading periodically
    while True:
        readings = ecoadapt.read_registers()
        mqtt.publish_ecoadapt_readings(readings)
        sleep(5)
