from pymodbus.server.sync import StartTcpServer
from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext

def build_data_list():
    ''' Create a list with the fake data from the sensor '''
    l = [514] + [2] + [30, 44285, 17639]
    l = l + [0] * (352-len(l)) + [49709, 17262, 20887, 15905, 45177, 15748, 0, 0, 0, 0, 0, 0]
    l = l + [0] * (388-len(l)) + [34030, 17262, 13400, 15907, 22707, 15748, 0, 0, 0, 0, 0, 0]
    l = l + [0] * (424-len(l)) + [54339, 16973, 54339, 16973, 43051, 16949, 0, 0, 0, 0, 0, 0]
    return l

def run_updating_server():
    ''' Create and start Modbus server '''
    data = build_data_list()
    store = ModbusSlaveContext(
        di=ModbusSequentialDataBlock(1, data),
        co=ModbusSequentialDataBlock(1, data),
        hr=ModbusSequentialDataBlock(1, data),
        ir=ModbusSequentialDataBlock(1, data)
        )
    context = ModbusServerContext(slaves=store, single=True)

    StartTcpServer(context=context, address=("0.0.0.0", 5020))


if __name__ == "__main__":
    ''' Simple Modbus server to fake sensor data '''
    run_updating_server()
