# Modbus Challenge
Communication of 4 services:  
modbus-server: Fake the data reading of the EcoAdapt sensor.  
modbus-bridge: Reads modbus data and send to server via MQTT.  
server-side:   Server subscribe to the sensor channels and write on file.  
mqtt-broker:   Simple mosquitto broker.  

To build and execute run the code below:
```bash
docker-compose up --build
```

Requirements:  
docker and docker-compose
