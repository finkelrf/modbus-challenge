#!/usr/bin/env python3
import json
import paho.mqtt.client as mqtt
from datetime import datetime

DATA_FILE = "/data/server-dump-data.txt"

class Mqtt():
    ''' Mqtt class '''
    def __init__(self, host, port=1883):
        self.mqtt_client = mqtt.Client()
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message
        self.mqtt_client.connect(host, port, 60)

    def loop(self):
        ''' Check for messages in the subscribed topics '''
        self.mqtt_client.loop_forever()

    def on_connect(self, client, userdata, flags, rc):
        ''' When connected subscribe to sensor topic '''
        print("Mqtt Connected!")
        self.mqtt_client.subscribe("rpi0/ecoadapt/+")

    def on_message(self, client, userdata, msg):
        ''' Dump received message to file '''
        payload = json.loads(msg.payload.decode())

        # Anything could be done here 
        # Launch threads to manipulate data
        # Save data to database
        # Call APIs of microservices that handle a specific functionality (eg. Low/High voltage alarms) 
        # But we are only printing the received message and writing to a file
        date_time = datetime.now()
        timestamp = datetime.timestamp(date_time)
        message = f"{timestamp}: {msg.topic} -> {str(payload)}\n"
        print(message)
        with open(DATA_FILE, 'a') as f:
            f.write(message)

if __name__ == "__main__":
    mqtt = Mqtt("mqtt-broker")

    mqtt.loop()
    